<?php
include_once('db_const.php');

function connection() {
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to Database ['.$conn->connect_errno.']');
    }
    
    return $conn;
}

function get_data() {
    $db = connection();
    $sql = "SELECT * FROM users";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
 }
    
    while($row = $result->fetch_assoc()) {
        $arr[] = array(
            "ID" => $row['id'],
            "Username" => $row['username'],
            "Password" => $row['password'],
            "FirstName" => $row['"first_name'],
            "LastName" => $row['last_name'],
            "Email" => $row['email']
        )
    }
    
    $json = json_encode(array($arr));
    
    $result->free();
    $db->close();
    
    return $json;
}

function showData($data, $page) {
    
    $array = json_decode($data, True);
    
    $output = "";
    
    if(count($array[0]) > 0) {
        for($i = 0; $i < count($array[0]); $i++)
        {
            if($page == "admin") {
                $output .= "<tr><td>".$array[0][$i]['Username']."</td><td>".$array[0][$i]
                ['Password']."</td><td>".$array[0][$i]['FirstName']."</td><td>".$array[0][$i]
                ['LastName']."</td><td>".$array[0][$i]['Email']."</td><td>";
            }
        }
        return $output;
    }
    else {
        $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
        
        return $output;
    }
}

function loadData($id) {

    $db = connection();
    $sql = "SELECT * FROM users WHERE ID = $id";
    $arr = [];

    $result = $db->query($sql);

    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "ID" => $row['id'],
                "Username" => $row['username'],
                "Password" => $row['password'],
                "FirstName" => $row['"first_name'],
                "LastName" => $row['last_name'],
                "Email" => $row['email']
        );
    }

    $json = json_encode(array($arr));

    $result->free();
    $db->close();

    return $json;
}

?>