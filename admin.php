<?php include_once('functions/functions.php'); ?>
<!doctype html>
<html>
    <head>
        <title>Admin</title>
        <link rel="stylesheet" href="css/main.css">
    </head>
    
    <body>	
        <header>
            <h1><a href="index.html">Exquisite <img src="images/logo.png" alt=""> Ink</a></h1>
            <h2 class="tagline">Tattoos || Piercings</h2>
            <nav>
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="register.php">Create Profile</a></li>
                    <li><a href="portfolio.html">Portfolio</a></li>
                    <li><a href="about.html">About Us</a></li>
                    <li><a href="contact.html">Contact Us</a></li>
                </ul>
            </nav>
        </header>
        
        <main>
            <article>
                <div class="content">
                    <h1>Users</h1>
                    <h2 class="left">Display User Information</h2>
                    <table>
                        <tr>
                            <th class="names">Username</th>
                            <th class="password">Password</th>
                            <th class="names">First Name</th>
                            <th class="names">Last Name</th>
                            <th class="email">Email</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        <?php echo showData(get_data(), "admin"); ?>
                    </table>
                    <h2 class="left"><a href="index.html">Index</a></h2>
                </div>
            </article>
            
            <article class="ad-ad2">
                        <a href="adduser.php" class="wanted">adduser</a>
                    </article>
            
            <article class="ad-ad2">
                        <a href="edituser.php" class="wanted">adduser</a>
            </article>
            
        </main>
        
        <footer>
            <section id="footerinfo">
                <small> ExquisiteInk </small>
                <p><a href="index.html"><img src="images/logo.png" alt="ExquisiteInk" width="70" height="70"></a></p>
            </section>
     </footer>
    </body>
</html>