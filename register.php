<html>
    <head>
        <title>User Registration</title>
        <link rel="stylesheet" href="css/main.css">
    </head>
    
    <body>	
        <header>
            <h1><a href="index.html">Exquisite <img src="images/logo.png" alt=""> Ink</a></h1>
            <h2 class="tagline">Tattoos || Piercings</h2>
            <nav>
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="register.php">Create Profile</a></li>
                    <li><a href="login.php">Login</a></li>
                    <li><a href="portfolio.html">Portfolio</a></li>
                    <li><a href="about.html">About Us</a></li>
                </ul>
            </nav>
        </header>
        
        <h1>User registration</h1>
        <?php
            require_once("db_const.php");
            if (!isset($_POST['submit'])) {
        ?>	<!-- The HTML registration form -->
            <form action="<?=$_SERVER['PHP_SELF']?>" method="post">
                <ul>
                    <li>
                      Username: <input type="text" name="username" />
                    </li>
                    <li>
                      Password: <input type="password" name="password" />
                    </li>
                    <li>
                       First name: <input type="text" name="first_name" />
                    </li>
                    <li>
                        Last name: <input type="text" name="last_name" />
                    </li>
                    <li>
                        Email: <input type="type" name="email" />
                    </li>
                </ul>
                <input type="submit" name="submit" value="Register" />
            </form>
            
            <footer>
                <section id="footerinfo">
                    <small> ExquisiteInk </small>
                    <p><a href="index.html"><img src="images/logo.png" alt="ExquisiteInk" width="70" height="70"></a></p>
                </section>
            </footer>
        <?php
        } else {
        
            $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
      
            if ($mysqli->connect_errno) {
                echo "<p>MySQL error no {$mysqli->connect_errno} : {$mysqli->connect_error}</p>";
                exit();
            }
    
    
            $username	= $_POST['username'];
            $password	= $_POST['password'];
            $first_name	= $_POST['first_name'];
            $last_name	= $_POST['last_name'];
            $email		= $_POST['email'];
        
   
            $exists = 0;
            $result = $mysqli->query("SELECT username from users WHERE username = '{$username}' LIMIT 1");
            if ($result->num_rows == 1) {
                $exists = 1;
                $result = $mysqli->query("SELECT email from users WHERE email = '{$email}' LIMIT 1");
                if ($result->num_rows == 1) $exists = 2;	
            } else {
                $result = $mysqli->query("SELECT email from users WHERE email = '{$email}' LIMIT 1");
                if ($result->num_rows == 1) $exists = 3;
            }
        
            if ($exists == 1) echo "<p>Username already exists!</p>";
            else if ($exists == 2) echo "<p>Username and Email already exists!</p>";
            else if ($exists == 3) echo "<p>Email already exists!</p>";
            else {
         
                $sql = "INSERT  INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `email`) 
                        VALUES (NULL, '{$username}', '{$password}', '{$first_name}', '{$last_name}', '{$email}')";
        
                if ($mysqli->query($sql)) {
                    echo "<p>Registered successfully!</p>";
                } else {
                    echo "<p>MySQL error no {$mysqli->errno} : {$mysqli->error}</p>";
                    exit();
                }
            }
        }
        ?>		
    </body>
</html>